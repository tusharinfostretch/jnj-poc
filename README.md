# Elasticsearch, Kibana, Beats

* change working directory to ```EK/```

## Deploy Elasticsearch and Kiabana

* create environment variable
  * ELASTIC_VERSION=6.2.3
* deploy containers

    ```bash
    docker-compose -f docker-compose-elasticsearch.yml up -d
    ```

## Deploy Beats

* create environment variable
  * ELASTIC_VERSION=6.2.3
  * ES_HOST=<elasticsearch-Host-IP>
  * ES_USERNAME=elastic
  * ES_PASSWORD=changeme
  * Eg.

    ```bash
    export ELASTIC_VERSION=6.2.3
    export ES_HOST=localhost
    export ES_USERNAME=elastic
    export ES_PASSWORD=changeme
    ```

* deploy containers

    ```bash
    docker-compose -f docker-compose-beats.yml up -d
    ```

# Monitoring - Prometheus, Alertmanager, Grafana

* change working directory to ```monitoring/```

## Deploy Prometheus, Alertmanager, Grafana

* deploy containers

    ```bash
    docker-compose -f docker-compose-monitorting.yml up -d
    ```